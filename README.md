# ESGF_Portal_React

The Search portal is a portal developed for searching cmip data from the ESGF data nodes
  
**DISCLAIMER:** Versions before `0.2.0` are unstable and not recommended for use.

## Dependencies

Make sure you have the following packages installed:

* React

## Installation

Open the terminal and enter `npm i @c4i/esgf-search`.

## Usage

1. Import SearchPortalApp: `import { SearchPortalApp } from '@c4i/esgf-search'`.
2. Add `<SearchPortalApp env="prod" />` where the search needs to be rendered.

## License

Copyright 2019 KNMI

  Licensed under the `Apache License`, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
