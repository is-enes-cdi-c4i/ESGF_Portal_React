# Readme for development

## Offline development

To develop offline ensure all of the following are true:

1. Make sure Search Portal is given `dev` as environment in the `stories/index.stories.js`-file: `<SearchPortalApp env="dev" />`
2. All dependencies are installed by using `npm install`
3. Json-server is running, using this command: `json-server --watch response.json --port 3000`. Usage of the --watch argument is optional.
4. Execute `npm run storybook` in a different terminal than the json-server
5. In storybook open the tab SearchPortal and there you will find the storybook component

### Troubleshooting

#### Json-server won't install!
If for some reason json-server is not installed after step 2, install json-server with `npm install json-server --save-dev`. If this fails, try `sudo npm install json-server -g`.

### Adding new offline-usable services

To add an offline-usable service, be sure to add the class' name to the Dependencies-object in `app.tsx`. This will enable  switching between which class is used, depending on the environment.

## Publishing

1. Write unit tests ;)
2. Run the unit tests, make sure they don't fail
3. Adjust version number in package.json
4. Commit your code
5. `npm run build`
6. `npm publish` (You need to authorize this machine using `npm adduser`)
