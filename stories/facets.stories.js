import {storiesOf} from '@storybook/react';
import React from 'react';
import BoundaryBox from '../src/js/components/expanded-property-finder/facets/boundary-box';
import TextSearch from '../src/js/components/expanded-property-finder/facets/text-search';
import YearRange from '../src/js/components/expanded-property-finder/facets/year-range';


storiesOf('Facets', module)
    .add('Time range', () => (
        <YearRange onSubmit={({min, max}) => alert(`[${min}-${max}]`)}/>
    ))
    .add('Boundary box', () => (
        <BoundaryBox/>
    ))
    .add('Text search', () => (
        <TextSearch/>
    ))
;

