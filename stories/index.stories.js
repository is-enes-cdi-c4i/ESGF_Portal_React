import {storiesOf} from '@storybook/react';
import React from 'react';
import SearchPortalApp from '../src/js/app';

/** @type {BackendUrlConfiguration} */
const devBackendUrlConfig = {
    searchUrl: 'http://localhost:3000',
    facetUrl: 'http://localhost:3000',
    catalogUrl: 'http://localhost:3000'
};

/** @type {BackendUrlConfiguration} */
const prodBackendUrlConfig = {
    searchUrl: 'http://jan-mouwes-2.knmi.nl:9000',
    facetUrl: 'http://jan-mouwes-2.knmi.nl:9000',
    catalogUrl: 'https://jan-mouwes-2.knmi.nl/adaguc-services/'
};

/** @type {BackendUrlConfiguration} */
const ec2BackendUrlConfig = {
    searchUrl: 'https://34.254.195.81/c4i-search-portal-backend/',
    facetUrl: 'https://34.254.195.81/c4i-search-portal-backend/',
    catalogUrl: 'https://34.254.195.81/adaguc-services/'
};

storiesOf('SearchPortaal', module)
    .add('Development', () => (
        <div>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
            <SearchPortalApp
                env="dev"
                backendUrls={devBackendUrlConfig}
            />
        </div>
    ))
    .add('Production', () => (
        <div>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
            <SearchPortalApp
                env="prod"
                backendUrls={ec2BackendUrlConfig}
            />
        </div>
    ))
;

