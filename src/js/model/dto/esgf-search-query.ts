import ESGFFilterPropertyDTO from './esgf-filter-property.dto';
import {DefaultPageInfo, PageInfo} from '../../data/backend-url.factory';

export default class EsgfSearchQuery {
    static emptyQuery(): EsgfSearchQuery {
        return new EsgfSearchQuery([]);
    }

    set specialFacets(value: Map<string, string>) {
        this._specialFacets = value;
    }

    set facetProperties(value: ESGFFilterPropertyDTO[]) {
        this._facetProperties = new Set<ESGFFilterPropertyDTO>(value);
    }

    set pageInfo(value: PageInfo) {
        this._pageInfo = value;
    }

    get pageInfo(): PageInfo {
        return this._pageInfo;
    }

    get specialFacets(): Map<string, string> {
        return this._specialFacets;
    }

    get facetProperties(): ESGFFilterPropertyDTO[] {
        return Array.from(this._facetProperties.values());
    }

    get id(): string {
        let map = this.facetProperties.reduce((map, {name, facet}) => {
            if (!map.has(facet.shortName)) map.set(facet.shortName, []);

            map.get(facet.shortName).push(name);

            return map;
        }, new Map<string, string[]>());

        const sortedKeys = Array.from(map.keys()).sort();
        const initialId = `search-index=${this._pageInfo.index}-size=${this._pageInfo.size}`;
        let id = Array.from(this._specialFacets.keys())
                      .sort()
                      .map(key => ({key: key, value: this._specialFacets.get(key)}))
                      .reduce((id, {key, value}) => `${id + key}=${value}`, initialId);

        return (String)(sortedKeys.map(key => ({facet: key, properties: map.get(key).sort()}))
                                  .reduce((currentId, {facet, properties}) => currentId + `${facet}[${properties.join(',')}],`, id));
    }

    private _pageInfo: PageInfo;
    private _facetProperties: Set<ESGFFilterPropertyDTO>;
    private _specialFacets: Map<string, string>;

    constructor(filterProperties: ESGFFilterPropertyDTO[], pageInfo?: PageInfo, specialFacets?: Map<string, string>) {
        this._facetProperties = new Set<ESGFFilterPropertyDTO>(filterProperties);
        this._pageInfo = pageInfo ? pageInfo : DefaultPageInfo;
        this._specialFacets = specialFacets ? specialFacets : new Map<string, string>();
    }
}
