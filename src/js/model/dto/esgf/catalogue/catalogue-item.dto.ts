export namespace Esgf {
    export namespace Catalogue {
        export interface Variable {
            name: string;
            vocabulary_name: string;
            units: string[];
            text: string;
        }

        export interface VariableContainer {
            variables: Variable[];
            vocabulary: string
        }

        export interface DataSize {
            units: string;
            amount: number;
        }

        export interface Dataset {
            url: URL;
            name: string;
            dataSize: DataSize;
            variables: VariableContainer;
            metadata: Map<string, string>;
        }

        /**
         * @summary Individual catalogue item. Contains all data, including metadata
         */
        export class Item {
            get datasets(): Dataset[] { return this._datasets; }

            private readonly _datasets: Dataset[];

            constructor(datasets: Dataset[]) {
                this._datasets = datasets;
            }

            private static parseDatasets(json: any[]): Dataset[] {

                const isUrl = item => typeof item == 'string'
                    && ((<string>item).startsWith('http://') || (<string>item).startsWith('https://'));

                const findUrls = (object) => Object.values(object)
                                                   .filter(isUrl)
                                                   .map(item => <string>item);

                const parseDataset = ({dataSize, text, variables, ...otherVars}) => {
                    const mapReducer = (previous: Map<string, string>,
                                        [key, value]: [string, any]) => previous.set(key, <string>value);
                    const metadataMap = Object.entries(otherVars)
                                              .reduce(mapReducer, new Map<string, string>());

                    let urls = findUrls(otherVars);

                    return {
                        name: text,
                        dataSize: dataSize,
                        variables: variables,
                        url: (urls.length > 0) ? new URL(urls[0]) : undefined,
                        metadata: metadataMap
                    };
                };

                return json.filter(({dataSize}) => dataSize != null)
                           .map(parseDataset);
            }

            public static parse(json: any): Item {
                let {files: json_dataset_list} = json;

                let dataset_list = this.parseDatasets(json_dataset_list);

                return new Item(dataset_list);
            }
        }
    }
}
