import {ESGFFilterDTO} from './esgf-filter.dto';

export default class ESGFFilterPropertyDTO {
    get facet(): ESGFFilterDTO {
        return this._facet;
    }

    set facet(value: ESGFFilterDTO) {
        this._facet = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    private _name: string;
    private _facet: ESGFFilterDTO;

    /**
     * @param {string} name
     * @param {ESGFFilterDTO} esgfFilter
     */
    constructor(name, esgfFilter) {
        this._name = name;
        this._facet = esgfFilter;
    }

    toString(): string {
        return this._name;
    }

    equals(otherFacetProperty: ESGFFilterPropertyDTO): boolean {
        return this.name === otherFacetProperty.name
            && this.facet.equals(otherFacetProperty.facet);
    }

}
