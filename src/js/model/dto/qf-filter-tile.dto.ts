import ESGFFilterPropertyDTO from "./esgf-filter-property.dto";

export class QFFilterTileDTO {
    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get color(): string {
        return this._color;
    }

    set color(value: string) {
        this._color = value;
    }

    get icon(): string {
        return this._icon;
    }

    set icon(value: string) {
        this._icon = value;
    }

    get properties(): ESGFFilterPropertyDTO[] {
        return this._properties;
    }

    set properties(value: ESGFFilterPropertyDTO[]) {
        this._properties = value;
    }

    private _title: string;
    private _color: string;
    private _icon: string;
    private _properties: ESGFFilterPropertyDTO[];

    /**
     *
     * @param {string}title
     * @param {ESGFFilterPropertyDTO[]}properties
     * @param {string}color
     * @param {string}icon
     */
    constructor(title = undefined, color = undefined, icon = undefined, properties = []) {
        this._title = title;
        this._color = color;
        this._icon = icon;
        this._properties = properties;
    }
}
