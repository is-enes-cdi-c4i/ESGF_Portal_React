import ESGFFilterPropertyDTO from './esgf-filter-property.dto';

export class ESGFFilterDTO {
    get properties(): ESGFFilterPropertyDTO[] {
        return this._properties;
    }

    set properties(value: ESGFFilterPropertyDTO[]) {
        this._properties = value;
    }

    get propertyCount(): number {
        return this._propertyCount;
    }

    set propertyCount(value: number) {
        this._propertyCount = value;
    }

    get shortName(): string {
        return this._shortName;
    }

    set shortName(value: string) {
        this._shortName = value;
    }

    private _shortName: string;
    private _propertyCount: number;
    private _properties: ESGFFilterPropertyDTO[];

    //NOTE temp class probably

    constructor(shortName: string = undefined, itemCount: number = undefined, properties: ESGFFilterPropertyDTO[] = []) {
        this._shortName = shortName;
        this._propertyCount = itemCount;
        this._properties = properties;
    }

    equals(otherFacet: ESGFFilterDTO): boolean {
        return this._shortName === otherFacet._shortName;
    }
}
