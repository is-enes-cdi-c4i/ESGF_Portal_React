import * as React from 'react';
import {Component} from 'react';
import {ESGFSearchPortal} from './components/esgf-search-portaal/esgf-search-portal.component';
import XPFWrapper from './components/expanded-property-finder/wrapper/xpf-wrapper.component';
import {QFWrapper} from './components/quick-filter-search/wrapper/qf-wrapper.component';
import {FacetProvider} from './data/esgf/facet/facet.provider';
import {ESGFSearchResultsProvider} from './data/esgf-search/esgf-search-results.provider';
import {QFTileProvider} from './data/qf-tile/qf-tile.provider';
import FacetService from './data/esgf/facet/facet.service';
import ESGFSearchService from './data/esgf-search/esgf-search.service';
import IFacetService from './data/esgf/facet/facet.service.interface';
import {ResultWrapper} from './components/results-search/result-wrapper/result-wrapper.component';
import BackendUrlFactory, {PageInfo} from './data/backend-url.factory';
import ESGFFilterPropertyDTO from './model/dto/esgf-filter-property.dto';
import EsgfSearchManager from './managers/esgf-search.manager';
import QFCWrapper from './components/quick-filter-customizer/wrapper/qfc-wrapper.component';
import ICatalogService from './data/catalog/catalog.service.interface';
import CatalogService from './data/catalog/catalog.service';
import ICatalogProvider from './data/catalog/catalog.provider.interface';
import CatalogProvider from './data/catalog/catalog.provider';
import IESGFSearchService from './data/esgf-search/esgf-search.service.interface';
import EsgfSearchQuery from './model/dto/esgf-search-query';
import {EsgfFacetServiceLocal} from './data/esgf/facet/esgf-filter.service.local';
import {ESGFSearchServiceLocal} from './data/esgf-search/esgf-search.service.local';
import CatalogServiceLocal from './data/catalog/catalog.service.local';
import SelectionManager from './managers/selection.manager';
import ISelectionManager from './managers/selection-manager.interface';
import BoundaryBox, {BoundaryBoxValue} from './components/expanded-property-finder/facets/boundary-box';
import YearRange, {YearRangeValue} from './components/expanded-property-finder/facets/year-range';
import SpecialFacets from './components/expanded-property-finder/facets/text-search';
import {isNullOrEmpty} from './util/string.util';
import {BackendUrlConfiguration} from './data/backend-url.factory.interface';
import {Esgf} from './model/dto/esgf/catalogue/catalogue-item.dto';
import Catalogue = Esgf.Catalogue;

interface AppEnvironment {
    CatalogueService: any,
    FilterService: any,
    FilterProvider: any,
    SearchService: any,
    SearchResultsProvider: any,
    SelectedPropertyManager: any,
    QFTileService: any,
    QuickFilterManager: any,
    QuickFilterTileProvider: any
}

const Dependencies = {
    dev: {
        CatalogueService: CatalogServiceLocal,
        SearchService: ESGFSearchServiceLocal,
        FilterService: EsgfFacetServiceLocal,
        SearchResultsProvider: ESGFSearchResultsProvider,
        FilterProvider: FacetProvider,
        QuickFilterTileProvider: QFTileProvider

    },
    prod: {
        CatalogueService: CatalogService,
        SearchService: ESGFSearchService,
        FilterService: FacetService,
        SearchResultsProvider: ESGFSearchResultsProvider,
        FilterProvider: FacetProvider,
        QuickFilterTileProvider: QFTileProvider
    }
};

type Environment = 'prod' | 'dev';
export type ViewDatasetHandler = (metadata: Catalogue.Dataset) => Promise<void>;

type Props = { env: Environment, backendUrls: BackendUrlConfiguration, viewDatasetHandler?: ViewDatasetHandler }

class SearchPortalApp extends Component<Props> {
    private readonly catalogService: ICatalogService;
    private readonly catalogProvider: ICatalogProvider;
    private readonly facetService: IFacetService;
    private readonly facetProvider: FacetProvider;
    private readonly searchService: IESGFSearchService;
    private readonly searchResultProvider: ESGFSearchResultsProvider;
    private readonly searchManager: EsgfSearchManager;
    private readonly selectedPropertyManager: ISelectionManager<ESGFFilterPropertyDTO>;
    private readonly specialFacetManager: ISelectionManager<{ facetName: string, value, toString: () => string }>;
    private readonly adagucUrlBuilder: BackendUrlFactory;

    private readonly env: Environment;
    private readonly searchBackendUrl: BackendUrlConfiguration;
    private readonly viewDatasetHandler: ViewDatasetHandler;

    constructor(props: Props) {
        super(props);

        this.env = props.env;
        this.searchBackendUrl = props.backendUrls;
        this.viewDatasetHandler = props.viewDatasetHandler || (async () => alert('This action is not currently supported'));

        let {
            FilterService,
            FilterProvider,
            SearchService,
            SearchResultsProvider,
            CatalogueService
        } = Dependencies[this.env] as AppEnvironment;

        this.adagucUrlBuilder = new BackendUrlFactory(this.searchBackendUrl);

        this.catalogService = new CatalogueService(this.adagucUrlBuilder);
        this.catalogProvider = new CatalogProvider(this.catalogService);

        this.searchService = new SearchService(this.adagucUrlBuilder);
        this.searchResultProvider = new SearchResultsProvider(this.searchService);
        this.searchManager = new EsgfSearchManager(this.searchResultProvider);

        this.facetService = new FilterService(this.adagucUrlBuilder);
        this.facetProvider = new FilterProvider(this.facetService);

        this.selectedPropertyManager = new SelectionManager<ESGFFilterPropertyDTO>();
        this.specialFacetManager = new SelectionManager<{ facetName, value }>();

        this.update = this.update.bind(this);
    }

    // Lifecycle methods

    update(): void {
        this.forceUpdate();
    }

    componentDidMount(): void {
        let setFilters = newFilters => this.setState(() => ({
            filters: Array.from(newFilters.values())
        }));

        this.facetProvider.provideMany()
            .then(setFilters);
    }

    render(): JSX.Element {
        let selectionChangedCallbacks = {
            property: (selection: ESGFFilterPropertyDTO[]) => (this.searchManager.currentQuery.facetProperties = selection) && this.searchManager.search(),
            specialFacets: (selection: { facetName: string, value: string }[]) => (this.searchManager.currentQuery.specialFacets = selection.reduce((map, {facetName, value}) => map.set(facetName, value), new Map<string, string>()))
                && this.searchManager.search()
        };
        let selectPage = (pageIndex: number, pageSize?: number) => {
            let {facetProperties: properties, pageInfo: oldPageInfo} = this.searchManager.currentQuery;

            let pageInfo: PageInfo = {index: pageIndex, size: pageSize || oldPageInfo.size};

            return this.searchManager.search(new EsgfSearchQuery(properties, pageInfo));
        };
        this.selectedPropertyManager.events.selectionChanged.subscribe(selectionChangedCallbacks.property);
        this.specialFacetManager.events.selectionChanged.subscribe(selectionChangedCallbacks.specialFacets);

        let createSpecialFacet = (facetName) => ({facetName: facetName, value: '', toString: () => facetName});

        let selectBbox = (value: BoundaryBoxValue) => {
            let facet = createSpecialFacet('bbox');
            if (this.specialFacetManager.isSelected(facet)) {
                this.specialFacetManager.deselect(facet);
            }

            facet.value = `[${[value.west, value.south, value.east, value.north].join(',')}]`;
            this.specialFacetManager.select(facet);
        };
        let selectTextQuery = (value: SpecialFacets.TextSearchValue) => {
            let facet = createSpecialFacet('query');
            this.specialFacetManager.deselect(facet);
            if (isNullOrEmpty(value)) {
                return;
            }
            facet.value = value;
            this.specialFacetManager.select(facet);
        };


        let selectYearRange = (value: YearRangeValue) => {
            let startFacet = createSpecialFacet('start');
            let endFacet = createSpecialFacet('end');

            this.specialFacetManager.deselect(startFacet);
            this.specialFacetManager.deselect(endFacet);

            let {min, max} = value;
            let formatDate = (year: number) => new Date(year, 1, 1).toISOString();

            let facets = [];
            if (min) {
                startFacet.value = formatDate(min);
                facets.push(startFacet);
            }
            if (max) {
                endFacet.value = formatDate(max);
                facets.push(endFacet);
            }
            this.specialFacetManager.selectMany(facets);

        };


        let specialFacetComponents = new Map<string, JSX.Element>([
            ['BoundBox', <BoundaryBox onSubmit={selectBbox}/>],
            ['Text', <SpecialFacets.TextSearch onSubmit={selectTextQuery}/>],
            ['Year', <YearRange onSubmit={selectYearRange}/>]
        ]);

        let QF = <QFWrapper selectionManager={this.selectedPropertyManager}
                            filterProvider={this.facetProvider}/>;

        let XPF = <XPFWrapper filterProvider={this.facetProvider}
                              specialFacetComponents={specialFacetComponents}
                              selectedPropertyManager={this.selectedPropertyManager}
                              specialFacetManager={this.specialFacetManager}/>;

        let QFC = <QFCWrapper filterProvider={this.facetProvider}/>;


        return (
            <div className='ESGFSearchPortal'>
                <ESGFSearchPortal
                    tabs={{'Quick filter': QF, 'Extended property finder': XPF, 'Quick filter customizer': QFC}}/>
                <ResultWrapper
                    catalogProvider={this.catalogProvider}
                    searchResultsManager={this.searchManager}
                    selectPage={selectPage}
                    viewClickHandler={this.viewDatasetHandler}
                />
            </div>
        );
    }
}

export default SearchPortalApp;
