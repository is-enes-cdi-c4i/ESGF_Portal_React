import ESGFDataNodeResultDTO from "../model/dto/esgf-data-node-result.dto";

export type BackendUrlConfiguration = { searchUrl: string, facetUrl: string, catalogUrl: string };

export default interface IBackendUrlFactory {
    createSearchUrl(): URL
    createFacetsUrl(): URL

    createCatalogUrl(catalogItem: ESGFDataNodeResultDTO): URL

    // /** @param {{specified: string[], mode: "blacklist" | "whitelist"}} config */
    // createFacetsUrl(config?: { specified: string[], mode: "blacklist" | "whitelist" }): URL;
    //
    // /** @param {string} shortName */
    // createFacetUrl(shortName: string): URL;
}
