import IBackendUrlFactory, {BackendUrlConfiguration} from './backend-url.factory.interface';
import ESGFDataNodeResultDTO from '../model/dto/esgf-data-node-result.dto';

export type PageInfo = { index: number, size: number };
export const DefaultPageInfo: PageInfo = {get index() { return 0;}, get size() { return 25;}};

export default class BackendUrlFactory implements IBackendUrlFactory {

    private readonly urlConfig: BackendUrlConfiguration;

    constructor(urlBase: BackendUrlConfiguration) {
        this.urlConfig = urlBase;
    }

    public createSearchUrl(): URL {
        const SEARCH_PATH = 'search';

        return new URL(SEARCH_PATH, this.urlConfig.searchUrl);
    }

    public createFacetsUrl(): URL {
        const SEARCH_PATH = 'facets';

        return new URL(SEARCH_PATH, this.urlConfig.facetUrl);
    }

    createCatalogUrl(catalogItem: ESGFDataNodeResultDTO): URL {
        const SEARCH_PATH = 'esgfsearch/catalog';

        const url = new URL(SEARCH_PATH, this.urlConfig.catalogUrl);
        url.searchParams.append('service', 'catalogbrowser');
        url.searchParams.append('node', catalogItem.url);
        url.searchParams.append('mode', 'flat');

        // url.searchParams.append("format", "text/html");

        return url;
    }
}
