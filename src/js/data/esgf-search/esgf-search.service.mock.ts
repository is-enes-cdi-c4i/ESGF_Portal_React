import {ESGFFilterDTO} from '../../model/dto/esgf-filter.dto';
import {range} from '../../util/array.util';
import IESGFSearchService from './esgf-search.service.interface';
import ESGFSearchResultDTO from '../../model/dto/esgf-search-result.dto';
import ESGFDataNodeResultDTO from '../../model/dto/esgf-data-node-result.dto';
import EsgfSearchQuery from '../../model/dto/esgf-search-query';

export class ESGFSearchServiceMock implements IESGFSearchService {

    private randomResults(): ESGFDataNodeResultDTO[] {
        return range(0, Math.ceil(Math.random() * 40) + 3)
            .map((n) => new ESGFDataNodeResultDTO(n, n, n + 'node', n + 'url'));
    }

    /**
     *
     * @return {Promise<ESGFFilterDTO[]>}
     */
    async fetch(query: EsgfSearchQuery): Promise<ESGFSearchResultDTO> {
        let results = this.randomResults();

        return new ESGFSearchResultDTO(results, results.length, 'query', 25);
    }

    async fetchAll(): Promise<ESGFSearchResultDTO> {
        let results = this.randomResults();
        return new ESGFSearchResultDTO(results.slice(0, 24), this.randomResults().length, '', 25);
    }
}
