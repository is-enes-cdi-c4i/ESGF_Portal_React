import ESGFSearchResultDTO from '../../model/dto/esgf-search-result.dto';
import EsgfSearchQuery from '../../model/dto/esgf-search-query';

export type SearchResultFactory = ({id, esgfid, data_node, url}) => ESGFSearchResultDTO;

export default interface IESGFSearchService {
    fetchAll(): Promise<ESGFSearchResultDTO>;

    fetch(query: EsgfSearchQuery): Promise<ESGFSearchResultDTO>;
}
