import ESGFSearchResultDTO from '../../model/dto/esgf-search-result.dto';
import IESGFSearchService from './esgf-search.service.interface';
import ESGFDataNodeResultDTO from '../../model/dto/esgf-data-node-result.dto';
import EsgfSearchQuery from '../../model/dto/esgf-search-query';

export class ESGFSearchServiceLocal implements IESGFSearchService {
    async fetch({facetProperties: properties, pageInfo}: EsgfSearchQuery): Promise<ESGFSearchResultDTO> {
        const url = 'http://localhost:3000/query';

        const createDTOs = (results) => results.map(({id, esgfid, data_node, url}) => new ESGFDataNodeResultDTO(id, esgfid, data_node, url));

        let response = await window.fetch(url, {
            method: 'GET'
        });

        if (!response.ok) return await Promise.reject();

        let {results} = await response.json();

        return new ESGFSearchResultDTO(createDTOs(results), results.length, '', results.length);
    }

    fetchAll(): Promise<ESGFSearchResultDTO> {
        return this.fetch(EsgfSearchQuery.emptyQuery());
    }
}
