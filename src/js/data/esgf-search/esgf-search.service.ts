import ESGFFilterPropertyDTO from '../../model/dto/esgf-filter-property.dto';
import IBackendUrlFactory from '../backend-url.factory.interface';
import IESGFSearchService from './esgf-search.service.interface';
import ESGFDataNodeResultDTO from '../../model/dto/esgf-data-node-result.dto';
import ESGFSearchResultDTO from '../../model/dto/esgf-search-result.dto';
import {PageInfo} from '../backend-url.factory';
import EsgfSearchQuery from '../../model/dto/esgf-search-query';

type SearchApiResponseDto = { results: { id, esgfId, dataNode, url }[], numberFound: number };
type SearchObject = { page_limit: number; facet_properties: {}[]; facet_blacklist: string[]; page: number };

export default class ESGFSearchService implements IESGFSearchService {
    private readonly _urlBuilder: IBackendUrlFactory;
    private readonly _activeRequests: Map<string, Promise<Response>>;

    private isRequestActive(id: string): boolean {
        return this._activeRequests.has(id);
    }

    private registerRequest(id: string, promise: Promise<Response>): void {
        this._activeRequests.set(id, promise);
    }

    private createSearchObject(properties: ESGFFilterPropertyDTO[], pageInfo: PageInfo, specialFacets: { facetName, value }[]): SearchObject {
        const facet_blacklist = ['citation_url'];

        let object = {
            facet_properties: properties.map((property: ESGFFilterPropertyDTO) => {
                let facetProperty = {};
                facetProperty[property.facet.shortName] = property.name;
                return facetProperty
            }),
            page: pageInfo.index,
            page_limit: pageInfo.size,
            facet_blacklist: facet_blacklist
        };

        object.facet_properties = specialFacets.reduce((currentArray, {facetName, value}) => {
            let object = {};
            object[facetName] = value;
            currentArray.push(object);
            return currentArray;
        }, object.facet_properties);

        return object;
    }

    constructor(urlBuilder: IBackendUrlFactory) {
        this._urlBuilder = urlBuilder;
        this._activeRequests = new Map<string, Promise<Response>>();
    }

    async fetchAll(): Promise<ESGFSearchResultDTO> {
        return await this.fetch(EsgfSearchQuery.emptyQuery());
    }

    async fetch(query: EsgfSearchQuery): Promise<ESGFSearchResultDTO> {
        const url = this._urlBuilder.createSearchUrl();

        let {facetProperties, pageInfo, id, specialFacets} = query;

        const searchObject = this.createSearchObject(facetProperties, pageInfo, Array.from(specialFacets.entries())
                                                                                     .map(([key, value]) => ({
                                                                                         facetName: key,
                                                                                         value: value
                                                                                     })));

        //  Register new request if not already sent
        if (!this.isRequestActive(id)) {
            let request: Promise<Response> = window.fetch(url.toString(),
                {
                    method: 'POST',
                    body: JSON.stringify(searchObject),
                    headers: {'Content-Type': 'application/json'}
                });
            this.registerRequest(id, request);
        }

        let response = await this._activeRequests.get(id);

        if (!response.ok) return await Promise.reject();

        let {results, numberFound} = await response.json() as SearchApiResponseDto;
        let resultDTOs = results.map(({id, esgfId, dataNode, url}) => new ESGFDataNodeResultDTO(id, esgfId, dataNode, url));

        return new ESGFSearchResultDTO(resultDTOs, numberFound);
    }
}
