import {ESGFFilterDTO} from "../../../model/dto/esgf-filter.dto";

export default interface IFacetService {

    /** @return {Promise<ESGFFilterDTO[]>} */
    fetchList(): Promise<ESGFFilterDTO[]>

    fetch(shortName: string): Promise<ESGFFilterDTO>
}
