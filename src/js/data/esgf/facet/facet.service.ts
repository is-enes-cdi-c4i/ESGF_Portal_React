import {ESGFFilterDTO} from '../../../model/dto/esgf-filter.dto';
import ESGFFilterFactory from '../../../model/factories/esgf-filter.factory';
import IFacetService from './facet.service.interface';
import IBackendUrlFactory from '../../backend-url.factory.interface';

export default class FacetService implements IFacetService {
    private readonly _filterFactory: ESGFFilterFactory;
    private readonly _urlBuilder: IBackendUrlFactory;

    private readonly _currentRequests: Map<string, Promise<ESGFFilterDTO>>;

    /**
     *
     * @param {AdagucUrlBuilder}urlBuilder
     */
    constructor(urlBuilder) {
        this.createFacetDto = this.createFacetDto.bind(this);

        this._currentRequests = new Map<string, Promise<ESGFFilterDTO>>();
        this._filterFactory = new ESGFFilterFactory();
        this._urlBuilder = urlBuilder;
    }

    private createFacetDto({shortName, properties}: { shortName: string, properties: string[], propertyCount: number }): ESGFFilterDTO {
        return this._filterFactory.create(shortName, properties);
    }

    /**
     * @return {Promise<ESGFFilterDTO[]>}
     */
    async fetchList(): Promise<ESGFFilterDTO[]> {

        const url = this._urlBuilder.createFacetsUrl();

        let response = await window.fetch(url.toString(), {method: 'GET'});
        if (!response.ok) return await Promise.reject();
        let facets = <{ shortName: string, properties: string[], propertyCount: number }[]>await response.json();

        return facets.map(this.createFacetDto);
    }

    /**
     *
     * @param id
     * @return {Promise<ESGFFilterDTO>}
     */
    async fetch(id: string): Promise<ESGFFilterDTO> {
        const url = `${this._urlBuilder.createFacetsUrl()}/${id}`;

        if (!this._currentRequests.has(id)) {
            this._currentRequests.set(id, window.fetch(url.toString(), {method: 'GET'})
                                                .then(response => response.ok ? response.json() : Promise.reject())
                                                .then(json => this.createFacetDto(json)));
        }

        let facet = await this._currentRequests.get(id);
        this._currentRequests.delete(id);
        return facet;
    }
}
