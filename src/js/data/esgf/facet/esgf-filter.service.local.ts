import ESGFFilterFactory from "../../../model/factories/esgf-filter.factory";
import IFacetService from "./facet.service.interface";
import {ESGFFilterDTO} from "../../../model/dto/esgf-filter.dto";
import IBackendUrlFactory from "../../backend-url.factory.interface";

export class EsgfFacetServiceLocal implements IFacetService {

    private readonly _urlFactory: IBackendUrlFactory;

    constructor(urlFactory: IBackendUrlFactory) {
        this._urlFactory = urlFactory;
    }

    /**
     * @summary returns list of filters
     */
    async fetchList(): Promise<ESGFFilterDTO[]> {
        const url = "http://localhost:3000/query";

        const facetFactory = new ESGFFilterFactory();

        const createDTOs = ({facets}) => Object.keys(facets)
                                               .map(facetName => facetFactory.create(facetName, Object.keys(facets[facetName])));

        return window.fetch(url, {method: "GET"})
                     .then(response => response.ok ? response.json() : Promise.reject())
                     .then(createDTOs);
    }

    async fetch(shortName: string): Promise<ESGFFilterDTO> {
        return (await this.fetchList()).filter(item => item.shortName === shortName)[0];
    }
}
