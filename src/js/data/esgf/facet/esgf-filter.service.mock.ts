import {ESGFFilterDTO} from "../../../model/dto/esgf-filter.dto";
import {range} from "../../../util/array.util";
import IFacetService from "./facet.service.interface";
import ESGFFilterPropertyDTO from "../../../model/dto/esgf-filter-property.dto";

export class ESGFFilterServiceMock implements IFacetService {

    /**
     *
     * @return {Promise<ESGFFilterDTO[]>}
     */
    async fetch(shortName: string): Promise<ESGFFilterDTO> {
        return new ESGFFilterDTO(shortName, 0, []);
    }

    fetchList(): Promise<ESGFFilterDTO[]> {
        let createRandomProperties = (number, filter) => range(0, Math.ceil(Math.random() * 10))
            .map(item => new ESGFFilterPropertyDTO("property" + item, filter));
        let createFilter = number => {
            let filter = new ESGFFilterDTO("test" + number, number ** 2, []);
            filter.properties = createRandomProperties(number, filter);
            return filter;
        };

        return new Promise(resolve => resolve(range(0, Math.ceil(Math.random() * 4) + 3).map(createFilter)));
    }
}
