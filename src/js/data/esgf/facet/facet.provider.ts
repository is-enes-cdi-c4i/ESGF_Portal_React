import {sleep} from '../../../util/async.util';
import {ESGFFilterDTO} from '../../../model/dto/esgf-filter.dto';
import ESGFFilterPropertyDTO from '../../../model/dto/esgf-filter-property.dto';
import FacetService from './facet.service';
import IFacetService from './facet.service.interface';

export class FacetProvider {
    private readonly filterService: IFacetService;
    private readonly facetCache: Map<String, ESGFFilterDTO>;
    private propertyCache: Map<ESGFFilterDTO, ESGFFilterPropertyDTO[]>;

    private fetching: boolean;

    /**
     * @param {FacetService} filterService
     */
    constructor(filterService) {
        this.filterService = filterService;
        this.facetCache = new Map();
        this.propertyCache = new Map();

        this.fetching = false;
    }

    get hasFilters(): boolean {
        return this.facetCache.size > 0;
    }

    /**
     * @return {Promise<Map<String, ESGFFilterDTO>>}
     */
    async provideMany(): Promise<Map<String, ESGFFilterDTO>> {
        while (this.fetching) {
            await sleep(10);
        }

        if (!this.hasFilters) {
            this.fetching = true;

            let cacheFilter = (filter: ESGFFilterDTO) => {
                this.facetCache.set(filter.shortName, filter);
                this.propertyCache.set(filter, filter.properties);
            };

            let filters = await this.filterService.fetchList();
            filters.forEach(cacheFilter); //TODO standardize to filter.id

            this.fetching = false;
        }

        return this.facetCache;
    }

    /**
     * @param facetName
     * @return {Promise<ESGFFilterDTO>}
     */
    async provide(facetName: string): Promise<ESGFFilterDTO> {
        /**
         * @param {ESGFFilterDTO}filter
         * @return {ESGFFilterDTO}
         */
        let provideFacetProperties = async (filter: ESGFFilterDTO) => {
            try {
                filter.properties = this.propertyCache.has(filter) ?
                    this.propertyCache.get(filter) :
                    (await this.filterService.fetch(filter.shortName)).properties;
            } catch (e) {
                throw e;
            }

            this.propertyCache.set(filter, filter.properties);
            return filter;
        };

        if (this.facetCache.has(facetName)) {
            return provideFacetProperties(this.facetCache.get(facetName));
        }
        let facet;
        try {
            facet = await this.filterService.fetch(facetName);
        } catch (e) {
            return await Promise.reject();
        }
        this.facetCache.set(facetName, facet);

        return provideFacetProperties(facet);
    }
}
