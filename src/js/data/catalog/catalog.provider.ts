import ICatalogProvider from './catalog.provider.interface';
import ESGFDataNodeResultDTO from '../../model/dto/esgf-data-node-result.dto';
import ICatalogService from './catalog.service.interface';
import {Esgf} from '../../model/dto/esgf/catalogue/catalogue-item.dto';
import CatalogItem = Esgf.Catalogue.Item;

export default class CatalogProvider implements ICatalogProvider {
    private readonly _cache: Map<string, CatalogItem>;
    private readonly _catalogService: ICatalogService;

    constructor(catalogService: ICatalogService) {
        this._catalogService = catalogService;
        this._cache = new Map<string, any>();
    }

    async provide(item: ESGFDataNodeResultDTO): Promise<CatalogItem> {
        let {esgfid: id} = item;

        if (!this._cache.has(id)) {
            let result;
            try {
                result = await this._catalogService.fetch(item);
            } catch (e) {
                return await Promise.reject();
            }
            this._cache.set(id, result);
        }

        return this._cache.get(id);
    }

}
