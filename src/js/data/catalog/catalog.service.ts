import ESGFDataNodeResultDTO from '../../model/dto/esgf-data-node-result.dto';
import BackendUrlFactory from '../backend-url.factory';
import ICatalogService from './catalog.service.interface';
import {Esgf} from '../../model/dto/esgf/catalogue/catalogue-item.dto';
import CatalogItem = Esgf.Catalogue.Item;

export default class CatalogService implements ICatalogService {
    private _urlBuilder: BackendUrlFactory;


    constructor(urlBuilder: BackendUrlFactory) {
        this._urlBuilder = urlBuilder;
    }

    async fetch(node: ESGFDataNodeResultDTO): Promise<CatalogItem> {
        let url = this._urlBuilder.createCatalogUrl(node);

        let response = await window.fetch(url.href);

        return CatalogItem.parse(await response.json());
    }
}
