import ESGFDataNodeResultDTO from '../../model/dto/esgf-data-node-result.dto';
import {Esgf} from '../../model/dto/esgf/catalogue/catalogue-item.dto';
import CatalogItem = Esgf.Catalogue.Item;

export default interface ICatalogService {
    fetch(node: ESGFDataNodeResultDTO): Promise<CatalogItem>
}
