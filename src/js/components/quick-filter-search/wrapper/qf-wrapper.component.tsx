import * as React from 'react';
import {Component} from 'react';
import StringFormatter from '../../../model/formatters/string.formatter';
import {QFSidebar} from '../qf-sidebar/qfsidebar.component';
import {QFTileProvider} from '../../../data/qf-tile/qf-tile.provider';
import ESGFFilterPropertyDTO from '../../../model/dto/esgf-filter-property.dto';
import {QFFilterTileDTO} from '../../../model/dto/qf-filter-tile.dto';
import LoadingIcons from '../../shared/icons/loading-icons.component';
import TileFactory from '../../../model/factories/tile.factory';
import {LocalStorageController} from '../../../data/localstorage/esgf-localstorage.controller';
import {QFFilterTileJSONDTO, QFTileConverter} from '../../../data/converters/qf-tile-converter';
import {FacetProvider} from '../../../data/esgf/facet/facet.provider';
import {QuickFilterTiles} from '../../../data/qf-tile/qf-tile-defaults.data';
import ISelectionManager from '../../../managers/selection-manager.interface';

enum ErrorState {
    NoError,
    ConnectionError,
    NotFoundError
}

export class QFWrapper extends Component<{ selectionManager: any, filterProvider: any }> {

    private readonly _selectedPropertyManager: ISelectionManager<ESGFFilterPropertyDTO>;
    private readonly _quickFilterProvider: QFTileProvider;
    private readonly _filterProvider: FacetProvider;
    private readonly _tileController: LocalStorageController<QFFilterTileDTO, QFFilterTileJSONDTO>;

    state: { QFSidebarShow: boolean, qfTileModels: Array<QFFilterTileDTO>, errorState: ErrorState };

    get componentContent() {
        let {qfTileModels, errorState} = this.state;

        if (errorState === ErrorState.ConnectionError) {
            return <LoadingIcons.NoConnection className={'text-danger m-auto'}
                                              onClick={() => this.update()}/>;
        }

        let qfTiles = this.createTiles(qfTileModels);
        let hasTiles = qfTiles.length > 0;

        return hasTiles ? qfTiles : <LoadingIcons.Spinner/>;
    }

    constructor(props) {
        super(props);

        this.togglePropertySelected = this.togglePropertySelected.bind(this);
        this.update = this.update.bind(this);
        this.quickFilterListItemFactory = this.quickFilterListItemFactory.bind(this);
        this.openNav = this.openNav.bind(this);
        this.closeNav = this.closeNav.bind(this);

        let {selectionManager, filterProvider, qfProvider} = props;
        this._selectedPropertyManager = selectionManager;
        this._quickFilterProvider = qfProvider;
        this._filterProvider = filterProvider;

        this._tileController = new LocalStorageController<QFFilterTileDTO, QFFilterTileJSONDTO>(new QFTileConverter(filterProvider), 'ESGFQFStorage', QuickFilterTiles.Defaults);

        this.state = {
            QFSidebarShow: false,
            qfTileModels: [],
            errorState: ErrorState.NoError
        };

    }

    openNav(): void {
        this.setState({QFSidebarShow: true});
    }

    closeNav(event: MouseEvent): void {
        this.setState({QFSidebarShow: false});
    }

    togglePropertySelected(property: ESGFFilterPropertyDTO): void {
        let {selected, select, deselect} = this._selectedPropertyManager;
        let isSelected = item => Array.from(selected.values())
                                      .some((entry) => entry.equals(item));

        (isSelected(property) ? deselect : select)(property);

        this.update();
    }

    /**
     *
     * @param {ESGFFilterPropertyDTO}item
     * @return {Component}
     * @constructor
     */
    quickFilterListItemFactory(item: ESGFFilterPropertyDTO): JSX.Element {
        let isSelected = item => Array.from(this._selectedPropertyManager.selected.values())
                                      .some((entry) => entry.equals(item));

        let selectProperty = () => this.togglePropertySelected(item);

        let createSliceWord = (nLetters: number) => (word: string) => word.split('')
                                                                          .slice(0, nLetters)
                                                                          .join('');

        let smallWord = createSliceWord(3)(item.facet.shortName);

        return (
            <li key={`${item.facet}-${item.name}`}
                className="qf-property"
                onClick={selectProperty}>
                <span className="name">
                    <input type="checkbox"
                           onChange={() => null} //prevents error message
                           checked={isSelected(item)}/>
                    {StringFormatter.toHumanText(item.name)} <span
                    className={'float-right text-right mr-1'}>({smallWord})</span>
                </span>
            </li>
        );
    };

    private async update(): Promise<void> {
        try {
            let qfTileModels = await Promise.all(this._tileController.getLocalstorage());
            this.setState({qfTileModels: qfTileModels});
        } catch (e) {
            this.setState({errorState: ErrorState.ConnectionError, error: e});
        }

    }

    componentDidMount(): void {
        this.update();
    }

    createTiles(qfTileModels: QFFilterTileDTO[]): JSX.Element[] {
        //TODO get with dependency injection
        let tileFactory = new TileFactory();

        if (qfTileModels.length === 0) return [];

        return qfTileModels.map(QFFilterTileDTO =>
            tileFactory.createTile(QFFilterTileDTO, this.quickFilterListItemFactory));
    }

    render(): JSX.Element {
        let {QFSidebarShow} = this.state;

        return (
            <section className="qf-wrapper">
                {QFSidebarShow ? <QFSidebar close={this.closeNav}/> : ''}
                {/*<div className="button-open-presets" onClick={this.openNav}>&#9776; Presets</div>*/}
                <div className="qf-main-container">
                    <div className="tiles">
                        {this.componentContent}
                    </div>
                </div>
            </section>
        );
    }

}
