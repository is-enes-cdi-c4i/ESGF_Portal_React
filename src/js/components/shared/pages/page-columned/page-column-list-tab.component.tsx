import React, {Component} from "react";
import SearchComponent from "../../../expanded-property-finder/wrapper/search.component";
import {SearchComponentModel} from "../../../../model/factories/page-column-tab.factory";
import UnorderedList from "../../list-unordered/list-unordered.component";
import LoadingIcons from "../../icons/loading-icons.component";
import IPageColumnTab, {PageColumnTabProps} from "./page-column-tab.interface";

type PageColumnListTabProps<TItem> = PageColumnTabProps & {
    items: TItem[],
    isLoading: boolean,
    listItemFactory,
    searchComponentModel: SearchComponentModel,
    sortFunction: (items: TItem[]) => TItem[]
}
type PageColumnListTabState<TItem> = { searchComponentModel: SearchComponentModel, searchQuery };

class PageColumnListTab<TItem = any> extends Component<PageColumnListTabProps<TItem>> implements IPageColumnTab {
    public get title(): string {
        return this._title;
    }

    public set title(value: string) {
        this._title = value;
    }

    private get renderItems(): TItem[] {
        let {searchComponentModel: {searchMethod}, searchQuery} = this.state;
        let {items, sortFunction} = this.props;

        return sortFunction(searchMethod(searchQuery, items));
    }

    private createListItem: (item: TItem) => JSX.Element;
    private _title: string;
    private _children: JSX.Element[];

    state: PageColumnListTabState<TItem>;

    constructor(props) {
        super(props);

        let {searchComponentModel, listItemFactory: createListItem} = props;

        this.createListItem = createListItem;

        this.state = {
            searchComponentModel: searchComponentModel,
            searchQuery: ""
        };

        this.handleSearch = this.handleSearch.bind(this);
    }

    /**
     * @param {string} query
     */
    handleSearch(query): void { this.setState({searchQuery: query}); }


    render(): JSX.Element {
        let {state: {searchComponentModel: {headerButtons}}, createListItem} = this;
        let {isLoading} = this.props;

        let content = (!isLoading) ?
            <UnorderedList className="List"
                           items={this.renderItems}
                           createListItem={createListItem}/> :
            <LoadingIcons.Spinner/>;

        this._children = [
            <SearchComponent onSearch={this.handleSearch}
                             key={"unique key"}
                             headerButtons={headerButtons}/>,
            content
        ];

        return <div className={this.props.className || ""}>{this._children}</div>;
    }
}

export default PageColumnListTab;
