import React, {Component} from 'react';

type Direction = 'north' | 'east' | 'south' | 'west';
export type BoundaryBoxValue = { north: number, east: number, south: number, west: number }
type Props = { onChange?: (value: BoundaryBoxValue) => void, onSubmit?: (value: BoundaryBoxValue) => void }
type State = { value: BoundaryBoxValue }

class BoundaryBox extends Component<Props> {

    state: State;

    constructor(props: Props) {
        super(props);

        this.state = {
            value: {north: 0, east: 0, south: 0, west: 0}
        }
    }

    onChange(direction: Direction, directionValue: number): void {
        let value = this.state.value;
        value[direction] = directionValue;

        this.setState({value: value});

        this.props.onChange != null && this.props.onChange(this.state.value);
    }

    render(): JSX.Element {

        return (
            <form className="Search">
                <p className="description"> Search box in degrees. Searches for data overlapping the box.</p>
                <table>
                    <tbody>
                    <tr>
                        <td/>
                        <td><input className="SearchBar BoundaryInput"
                                   type="number"
                                   placeholder="North"
                                   aria-label="North"
                                   onChange={(event) => this.onChange('north', Number(event.target.value))}/></td>
                        <td/>
                    </tr>
                    <tr>
                        <td><input className="SearchBar BoundaryInput"
                                   type="number"
                                   placeholder="East"
                                   aria-label="East"
                                   onChange={(event) => this.onChange('east', Number(event.target.value))}/>
                        </td>
                        <td/>
                        <td><input className="SearchBar BoundaryInput"
                                   type="number"
                                   placeholder="West"
                                   aria-label="West"
                                   onChange={(event) => this.onChange('west', Number(event.target.value))}/>
                        </td>
                    </tr>
                    <tr>
                        <td/>
                        <td><input className="SearchBar BoundaryInput"
                                   type="number"
                                   placeholder="South"
                                   aria-label="South"
                                   onChange={(event) => this.onChange('south', Number(event.target.value))}/></td>
                        <td/>
                    </tr>
                    </tbody>
                </table>
                <div className="button-container">
                    <input className="btn btn-success btn-sm" type="button" value="Search"
                           onClick={() => this.props.onSubmit(this.state.value)}/>
                    <input className="btn btn-danger btn-sm" type="reset" value="Clear"
                           onClick={() => this.setState({value: {north: 0, east: 0, south: 0, west: 0}})}/>
                </div>
            </form>
        );
    }
}

export default BoundaryBox;
