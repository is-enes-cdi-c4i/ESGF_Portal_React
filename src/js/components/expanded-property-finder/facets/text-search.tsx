import React, {Component} from 'react';

export namespace SpecialFacets {
    export type TextSearchValue = string;
    type TextSearchCallback = (TextSearchValue) => void;

    export class TextSearch extends Component<{ onChange?: TextSearchCallback, onSubmit?: TextSearchCallback }> {

        state: { value: string };

        constructor(props) {
            super(props);
            this.state = {
                value: ''
            }
        }

        render(): JSX.Element {

            let {onChange, onSubmit} = this.props;

            let SearchButton = () => (
                <div className="SearchButton">
                    <button className="Button ">
                        <i className="fas fa-search"/>
                    </button>
                </div>);

            let eventResponse = (event: React.ChangeEvent<HTMLInputElement>) => (this.state.value = event.target.value) && onChange && onChange(this.state.value);

            let handleSubmit = (event) => {
                event.preventDefault();
                onSubmit && onSubmit(this.state.value);
            };
            return (
                <form className="Search" onSubmit={handleSubmit}>
                    <p className="description"> The free text query can be used to execute a query that matches the
                        given text anywhere in the metadata fields.<br/><br/>
                        If you happen to know a property in the Solr metadata that gives an exact match, then you can
                        search for it using "name:value" and just get the results of interest.<br/><br/>
                        For example "drs_id:cmip5.output1.MPI-M.MPI-ESM-LR.abrupt4xCO2.day.atmos.cfDay.r1i1p1" will
                        return results matching the drs_id.
                    </p>
                    <input className="SearchBar"
                           type="text"
                           placeholder="Search"
                           onChange={eventResponse}
                           aria-label="Search"/>
                    <SearchButton/>
                </form>
            )   //TODO: Put the appropriate search function in the SearchButton
        }
    }
}

export default SpecialFacets;
