import React, {Component} from 'react';

export type YearRangeValue = { min: number | null, max: number | null };
type YearRangeCallback = (value: YearRangeValue) => void;
type State = { value: YearRangeValue };
type Props = { onChange?: YearRangeCallback, onSubmit?: YearRangeCallback, minYear?: number, maxYear?: number };

class YearRange extends Component<Props> {
    state: State;
    inputs: { from: HTMLInputElement, to: HTMLInputElement };

    constructor(props: Props) {
        super(props);

        this.reset = this.reset.bind(this);

        this.inputs = {from: null, to: null};

        this.state = {
            value: {
                min: null,
                max: null
            }
        };
    }

    private isValid(value: YearRangeValue): boolean {
        const {minYear, maxYear} = this.props;
        const {min: start, max: end} = value;

        if (minYear != null && start < minYear) return false;
        if (maxYear != null && end < maxYear) return false;

        return start <= end;
    }


    private reset(): void {
        Object.values(this.inputs)
              .forEach(input => input.value = "");
        this.setState({value: {min: null, max: null}})
    }

    render(): JSX.Element {

        type Key = 'min' | 'max';
        let changeValuePart = (key, value) => this.state.value[key] = value;
        let handleChange = (key: Key, number: number) => {
            changeValuePart(key, number);
            this.isValid(this.state.value);
            if (!this.props.onChange) return;
            this.props.onChange(this.state.value);
        };

        let buttons = [
            {
                refName: 'from',
                name: 'Search',
                type: 'button',
                displayType: 'success',
                onClick: () => this.props.onSubmit && this.props.onSubmit(this.state.value)
            },
            {
                refName: 'to',
                name: 'Clear',
                type: 'reset',
                displayType: 'danger',
                onClick: this.reset
            }
        ];

        return (
            <form>
                <div className="Search">
                    <p className="description">Specify the date in years you wish to search for. Results will be shown
                        that overlap the specified date.</p>
                    <label className="description time-description">From</label>
                    <input
                        className="SearchBar"
                        placeholder={'YYYY'}
                        type="number"
                        onChange={(event) => handleChange('min', Number(event.target.value))}
                    />
                    <label className="description time-description">until</label>
                    <input
                        className="SearchBar"
                        placeholder={'YYYY'}
                        type="number"
                        onChange={(event) => handleChange('max', Number(event.target.value))}
                    />
                </div>
                <div className="Search">
                    <div className="button-container">
                        {
                            buttons.map(({refName, name, displayType, onClick, type}) =>
                                <input
                                    ref={element => this.inputs[refName] = element}
                                    className={`btn btn-${displayType} btn-sm`}
                                    type={type}
                                    value={name}
                                    onClick={onClick}
                                />
                            )
                        }
                    </div>
                </div>
            </form>
        );
    }
}

export default YearRange;
