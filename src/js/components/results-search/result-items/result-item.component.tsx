import React, {Component} from 'react';
import Collapsible from 'react-collapsible';
import ResultItemOption from './result-item-option.component'
import ArrowIcons from '../../shared/icons/arrow-icons.component';
import ESGFDataNodeResultDTO from '../../../model/dto/esgf-data-node-result.dto';
import ICatalogProvider from '../../../data/catalog/catalog.provider.interface';
import LoadingIcons from '../../shared/icons/loading-icons.component';
import DatasetListItem from './result-dataset-list-item.component';
import {Esgf} from '../../../model/dto/esgf/catalogue/catalogue-item.dto';
import {ViewDatasetHandler} from '../../../app';
import CatalogItem = Esgf.Catalogue.Item;
import CatalogItemDataset = Esgf.Catalogue.Dataset;


enum ViewMode {
    Files = 'viewmode-files',
    Aggregates = 'viewmode-aggregates',
    Both = 'viewmode-both',
}

enum ErrorState {
    NoError,
    ConnectionError,
    NotFoundError
}

type Props = { labelModel: ESGFDataNodeResultDTO, resultProvider: ICatalogProvider, viewClickHandler: ViewDatasetHandler };
type State = { isOpen: boolean, contentModel: CatalogItem, viewMode: ViewMode, errorState: ErrorState };

export default class ResultItem extends Component<Props> {

    get renderDatasets() {
        let filterMethod = (ignored) => true;
        let isAggregate = (item: CatalogItemDataset) => item.name.endsWith('aggregation');

        switch (this.state.viewMode) {
            case ViewMode.Aggregates:
                filterMethod = isAggregate;
                break;
            case ViewMode.Files:
                filterMethod = (item: CatalogItemDataset) => !isAggregate(item);
        }

        return this.state.contentModel.datasets.filter(filterMethod);

    }

    private get contentComponent() {

        let {errorState, contentModel} = this.state;

        if (errorState != ErrorState.NoError) return <LoadingIcons.Error className={'text-danger'}/>;
        if (!contentModel) return <LoadingIcons.Spinner/>;

        let createTableRow = (dataset: CatalogItemDataset, index: number) => <DatasetListItem
            viewClickHandler={this.props.viewClickHandler}
            dataset={dataset}
            index={index}
        />;

        return this.renderDatasets.map(createTableRow);
    }

    state: State;

    constructor(props) {
        super(props);

        this.state = {
            isOpen: true,
            contentModel: null,
            viewMode: ViewMode.Both,
            errorState: ErrorState.NoError
        };

    }

    setArrowIsOpen(isOpen: boolean): void {
        this.setState({isOpen: isOpen});
    }

    private async fetchContentModel(): Promise<void> {
        try {
            let contentModel = await this.props.resultProvider.provide(this.props.labelModel);
            this.setState({contentModel: contentModel});
        } catch (e) {
            this.setState({errorState: ErrorState.ConnectionError});
        }
    }

    openUrl() {
        window.open(this.props.labelModel.url,'_blank');
    }

    copyUrl() {
        navigator.clipboard.writeText(this.props.labelModel.url);
    }

    render(): JSX.Element {
        let {isOpen} = this.state;
        let {labelModel} = this.props;

        let Arrow = isOpen ? ArrowIcons.Down : ArrowIcons.Up;

        let closeArrow = () => this.setArrowIsOpen(true);
        let openArrow = () => this.setArrowIsOpen(false);
        let handleOpen = () => {
            openArrow();
            return this.fetchContentModel();
        };

        let ViewModeSelector = ({options, selected, className = ''}: { options: Map<ViewMode, string>, selected: ViewMode, className }) => {
            let onChange = ({target: {value: viewMode}}) => this.setState({viewMode: viewMode});

            let optionArray = Array.from(options.entries());

            return <select
                className={'form-control-sm ' + className}
                name={'input-select-viewmode'}
                value={selected}
                onChange={onChange}
            >
                {optionArray.map(([viewMode, label]) => <option value={viewMode}>{label}</option>)}
            </select>;
        };

        let {esgfid} = labelModel;
        let content = this.contentComponent;

        return (
            <Collapsible lazyRender={true}
                         trigger={<div><Arrow/> {esgfid}</div>}
                         onOpening={handleOpen}
                         onClosing={closeArrow}>
                <form className={'form-inline mb-2'}>
                    <div className={'col-10'}>
                        <b>View mode:</b> <ViewModeSelector options={new Map([
                        [ViewMode.Both, 'Both'],
                        [ViewMode.Files, 'Files'],
                        [ViewMode.Aggregates, 'Aggregates']
                    ])} selected={this.state.viewMode} className={'mr-sm-2'}/>
                    </div>
                    <ResultItemOption labelModel={this.props.labelModel}/>
                </form>
                <table className="table">
                    <thead>
                    <th scope="col">#</th>
                    <th scope="col">Dataset</th>
                    <th scope="col">Size</th>
                    <th scope="col">Download</th>
                    <th scope="col">View</th>
                    <th scope="col">Basket</th>
                    </thead>
                    <tbody>
                    {content}
                    </tbody>
                </table>
            </Collapsible>
        );
    }
}
