import React, { Component } from 'react';
import ESGFDataNodeResultDTO from '../../../model/dto/esgf-data-node-result.dto';


type Props = { labelModel: ESGFDataNodeResultDTO};


export default class ResultItemOption extends Component<Props> {

    openUrl() {
        window.open(this.props.labelModel.url,'_blank');
    }

    copyUrl() {
        navigator.clipboard.writeText(this.props.labelModel.url);
    }

    constructor(props) {
        super(props);
    }

    render(): JSX.Element {
        return (
            <div className={'col-2'}>
                <b>Catalog url: </b>
                <i className={'far fa-folder-open ml-2 clickable'} title="Open" onClick={() => this.openUrl()}/>
                <i className={'fas fa-copy ml-2 clickable'} title="Copy to Clipboard" onClick={() => this.copyUrl()}/>
            </div>
        );
    }
}