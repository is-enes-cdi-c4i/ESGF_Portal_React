import ResultIcons from '../../shared/icons/result-icons.component';
import React from 'react';
import {Esgf} from '../../../model/dto/esgf/catalogue/catalogue-item.dto';
import {ViewDatasetHandler} from '../../../app';
import CatalogItemDataset = Esgf.Catalogue.Dataset;

type Props = { dataset: CatalogItemDataset, index: number, viewClickHandler: ViewDatasetHandler }

export default function DatasetListItem(props: Props) {
    let {dataset, index, viewClickHandler} = props;

    let downloadLink = dataset.url ?
        <a target={'_blank'}
           href={dataset.url.href}
           className={'clickable'}>
            <ResultIcons.Download className={'mx-auto'}/>
        </a> :
        '-';

    let notImplementedAlert = () => alert('Not yet implemented, we\'re working on it!'); //TODO remove this, beta only

    return (
        <tr>
            <th scope="row">{index + 1}</th>
            <td>{dataset.name}</td>
            <td>{dataset.dataSize}</td>
            <td className="text-center">{downloadLink}</td>
            <td className="clickable text-center" onClick={() => viewClickHandler(dataset)}>View</td>
            <td className="clickable text-center" onClick={notImplementedAlert}>
                <ResultIcons.Basket/>
            </td>
        </tr>
    );
}
;
