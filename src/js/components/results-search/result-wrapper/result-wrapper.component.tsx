import React, {Component} from 'react';
import ResultItem from '../result-items/result-item.component';
import EsgfSearchManager from '../../../managers/esgf-search.manager';
import ESGFDataNodeResultDTO from '../../../model/dto/esgf-data-node-result.dto';
import ESGFSearchResultDTO from '../../../model/dto/esgf-search-result.dto';
import LoadingIcons from '../../shared/icons/loading-icons.component';
import EsgfSearchQuery from '../../../model/dto/esgf-search-query';
import ICatalogProvider from '../../../data/catalog/catalog.provider.interface';
import PageSelector from '../../shared/paging/page-selector.component';
import {DefaultPageInfo, PageInfo} from '../../../data/backend-url.factory';
import {ViewDatasetHandler} from '../../../app';

type ResultWrapperProps = { catalogProvider: ICatalogProvider, searchResultsManager: EsgfSearchManager, selectPage: (index: number) => Promise<any>, defaultCurrentPage?: number, viewClickHandler: ViewDatasetHandler };

type State = { searchResult: ESGFSearchResultDTO, isLoading: boolean, currentPageIndex: number, searchPageInfo: PageInfo }

export class ResultWrapper extends Component<ResultWrapperProps> {
    private _searchManager: EsgfSearchManager;

    state: State;

    constructor(props: ResultWrapperProps) {
        super(props);
        this.forceUpdate = this.forceUpdate.bind(this);
        this.setResults = this.setResults.bind(this);
        this.setLoading = this.setLoading.bind(this);

        let {searchResultsManager, defaultCurrentPage} = props;

        this._searchManager = searchResultsManager;

        this.state = {
            searchResult: searchResultsManager.currentResults,
            isLoading: false,
            currentPageIndex: defaultCurrentPage || 0,
            searchPageInfo: this._searchManager.currentQuery ?
                this._searchManager.currentQuery.pageInfo :
                DefaultPageInfo
        };

        this._searchManager.events.searchStarted.subscribe(this.setLoading);
        this._searchManager.events.searched.subscribe(this.setResults);
    }

    setLoading(isLoading = true) {
        this.setState(() => ({isLoading: isLoading}));
    }

    setResults(searchResult): void {
        this.setState(() => ({
            searchResult: searchResult,
            isLoading: false,
            searchPageInfo: this._searchManager.currentQuery.pageInfo
        }));
    }

    componentDidMount(): void {
        this._searchManager.search(new EsgfSearchQuery([]));
    }


    render() {
        let {searchResult, isLoading, searchPageInfo} = this.state;

        isLoading = isLoading || !searchResult;

        let createResultItem = (result: ESGFDataNodeResultDTO) => <ResultItem
            key={result.esgfid}
            labelModel={result}
            resultProvider={this.props.catalogProvider}
            viewClickHandler={this.props.viewClickHandler}
        />;

        let maxPage = !isLoading ? Math.ceil(searchResult.numfound / searchPageInfo.size) : 1;

        let resultComponents = !isLoading ? searchResult.results.map(createResultItem) : <LoadingIcons.Spinner/>;
        let numFoundComponent = !isLoading ?
            `(${new Intl.NumberFormat('en-US').format(searchResult.numfound)})` :
            <LoadingIcons.SpinnerInline/>;
        let maxPageComponent = !isLoading ? ' - displaying page ' + (1) + ' of ' + maxPage : null;


        return (
            <div className="result-wrapper">
                <div className="result-header">
                    Results {numFoundComponent}{maxPageComponent}
                    <PageSelector
                        onPageChange={index => this.props.selectPage(index - 1)}
                        maxPage={maxPage}
                        className={'float-right'}
                    />
                </div>
                {resultComponents}
            </div>
        );
    }
}
