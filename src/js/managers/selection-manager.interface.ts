import {EventSubscriber} from '../lib/event-emitter/events';

export type SelectionEvents<T = any> = { readonly selectionChanged: EventSubscriber<T[]>; readonly deselected: EventSubscriber<T[]>; readonly selected: EventSubscriber<T[]> }
export default interface ISelectionManager<T> {
    selected: Set<T>;

    readonly events: SelectionEvents<T>;

    select(item: T): void;

    selectMany(items: T[]): void;

    deselect(item: T): void;

    deselectMany(items: T[]): void;

    toggle(item: T): void;

    clear(): void;

    isSelected(item: T): boolean;
}
