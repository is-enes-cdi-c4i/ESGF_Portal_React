import {EventEmitter, EventSubscriber} from '../lib/event-emitter/events';
import ISelectionManager, {SelectionEvents} from './selection-manager.interface';

enum Events {
    SelectionChanged = 'selection-changed',
    Selected = 'property-selected',
    Deselected = 'property-deselected'
}

class SelectionManager<T> implements ISelectionManager<T> {
    /**
     * @summary returns current selection
     */
    get selected(): Set<T> {
        return new Set<T>(this.selection.values())
    }

    set selected(selection: Set<T>) {
        this.selection.clear(); //not using this.clear() because it emits an event
        this.selectMany(Array.from(selection));
    }

    private readonly eventEmitter: EventEmitter;
    private readonly selection: Map<String, T>;

    public readonly events: SelectionEvents;

    constructor() {
        this.clear = this.clear.bind(this);
        this.isSelected = this.isSelected.bind(this);
        this.select = this.select.bind(this);
        this.deselect = this.deselect.bind(this);
        this.selectMany = this.selectMany.bind(this);
        this.deselectMany = this.deselectMany.bind(this);
        this.toggle = this.toggle.bind(this);

        this.selection = new Map<String, T>();

        this.eventEmitter = new EventEmitter();

        this.events = {
            selectionChanged: new EventSubscriber(Events.SelectionChanged, this.eventEmitter),
            selected: new EventSubscriber(Events.Selected, this.eventEmitter),
            deselected: new EventSubscriber(Events.Deselected, this.eventEmitter)
        };

    }

    select(item: T): void {
        //  Calls selectMany because if it were to be the other way around,
        //      too many events would be thrown
        this.selectMany([item]);
    };

    selectMany(items: T[]): void {
        let select = item => !this.isSelected(item) && this.selection.set(item.toString(), item);

        items.forEach(select);

        this.eventEmitter.emit(Events.SelectionChanged, Array.from(this.selected));
        this.eventEmitter.emit(Events.Selected, Array.from(this.selected));
    };

    /**
     * @summary deselects item
     */
    deselect(item: T): void {
        //  Calls selectMany because if it were to be the other way around,
        //      too many events would be thrown
        this.deselectMany([item]);
    };

    deselectMany(items: T[]): void {
        const deselect = item => this.isSelected(item) && this.selection.delete(item.toString());

        items.forEach(deselect);

        this.eventEmitter.emit(Events.SelectionChanged, Array.from(this.selected));
        this.eventEmitter.emit(Events.Deselected, Array.from(this.selected));
    };

    toggle(item: T): void {
        let {select, deselect, isSelected} = this;

        (isSelected(item) ? deselect : select)(item);
    }

    /**
     * @summary Deselects all items.
     */
    clear(): void {
        this.deselectMany(Array.from(this.selected));
    }

    isSelected(item: T): boolean {
        return this.selection.has(item.toString());
    }
}

export default SelectionManager;
