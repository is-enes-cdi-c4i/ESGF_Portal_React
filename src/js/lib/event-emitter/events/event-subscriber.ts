type EventCallback<T> = (item: T) => void

/**
 * @package
 */
class EventSubscriber<T = any> {
    public readonly subscribe: (callback: EventCallback<T>) => void;
    public readonly unsubscribe: (callback: EventCallback<T>) => void;

    /**
     *
     * @param {string} eventName
     * @param {EventEmitter} eventEmitter
     * @constructor
     */
    constructor(eventName, eventEmitter) {
        this.subscribe = (callback: EventCallback<T>) => eventEmitter.subscribe(eventName, callback);
        this.unsubscribe = (callback: EventCallback<T>) => eventEmitter.unsubscribe(eventName, callback);
    }
}

export default EventSubscriber;
