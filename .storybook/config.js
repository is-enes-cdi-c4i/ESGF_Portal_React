import { configure } from '@storybook/react';

import "../src/style/ESGFSearch.scss";

const req = require.context('../stories', true, /\.stories\.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}
// function loadStories() {
//   require('../stories/**/*.js');
//   // You can require as many stories as you need.
// }

configure(loadStories, module);
