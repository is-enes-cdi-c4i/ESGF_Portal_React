import EsgfSearchQuery from '../../../src/js/model/dto/esgf-search-query';
import { ESGFFilterDTO } from '../../../src/js/model/dto/esgf-filter.dto';
import ESGFFilterPropertyDTO from '../../../src/js/model/dto/esgf-filter-property.dto';
import { DefaultPageInfo } from '../../../src/js/data/backend-url.factory';
import { string } from 'prop-types';

// Running tests about the search query
describe('SearchQuery', () => {
    let standardQuery = "search-index=0-size=25";
    let testShortName = "test";
    let testName = "test-name";

    // Premade creation of Filter and Property so it's easier to call in the tests
    let createMap = () => new Map<string, string>();
    let createFilter = (id?: string) => {
        if(id == undefined) return new ESGFFilterDTO(testShortName, 1, []);
        else return new ESGFFilterDTO(testShortName + id, 1, []);
    };
    let createProperty = (filter: ESGFFilterDTO, id?: string) => {
        if(id == undefined) return new ESGFFilterPropertyDTO(testName, filter);
        else return new ESGFFilterPropertyDTO(testName + id, filter);
    }

    test('empty query should return the full query', () => {
        // Creating an empty query
        const emptyQuery = new EsgfSearchQuery([]);
        const actual = emptyQuery.id;

        // Running the test
        expect(actual).toBe(standardQuery);
    });

    test('query with empty filter should return the full query', () => {
        // Creating an empty filter and including it in a property
        const emptyFilter = new ESGFFilterDTO();
        const esgfFilterProperty = createProperty(emptyFilter);

        // Creating the query
        const emptyFilterQuery = new EsgfSearchQuery(esgfFilterProperty[0]);
        const actual = emptyFilterQuery.id;

        // Running the test
        expect(actual).toBe(standardQuery);
    });

    test('empty facetProperties given should return an empty array', () => {
        // Creating an empty query
        const emptyFacetPropertyQuery = new EsgfSearchQuery([]);

        // Creating and setting empty facet properties
        const emptyFacetProperties = null;
        emptyFacetPropertyQuery.facetProperties = emptyFacetProperties;

        // Expected query and the actual
        const expected = [];
        const actual = emptyFacetPropertyQuery.facetProperties;

        // Running the test
        expect(actual).toStrictEqual(expected);
    });

    test('custom filter search should return the query with the custom filter', () => {
        // Creating the filter and property
        const filter = createFilter();
        const property = createProperty(filter);

        // Put the property in an array and then putting it in an query
        const properties = [property];
        const filterAndPropertyQuery = new EsgfSearchQuery(properties);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + "[" + testName + "],";
        const actual = filterAndPropertyQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });

    test('multiple custom filters search should return the query with the custom filters', () => {
        // Creating Filters
        const filter1 = createFilter("1");
        const filter2 = createFilter("2");
        const filter3 = createFilter("3");

        // Creating the properties and putting them in an array
        const property1 = createProperty(filter1);
        const property2 = createProperty(filter2);
        const property3 = createProperty(filter3);
        const properties = [property1, property2, property3];

        // Creating the query with the properties
        const multipleFilterQuery = new EsgfSearchQuery(properties);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + "1[" + testName + "],"
                                            + testShortName + "2[" + testName + "],"
                                            + testShortName + "3[" + testName + "],";
        const actual = multipleFilterQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });

    test('multiple custom properties with one filter search should return the query with the custom filters', () => {
        // Creating Filters
        const filter = createFilter();

        // Creating the properties and putting them in an array
        const property1 = createProperty(filter);
        const property2 = createProperty(filter);
        const property3 = createProperty(filter);
        const properties = [property1, property2, property3];

        // Creating the query with the properties
        const multiplePropertyQuery = new EsgfSearchQuery(properties);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + "[" + testName + ',' + testName + ',' + testName + "],";
        const actual = multiplePropertyQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });

    test('query without any filterProperties should just work and like normal return the query', () => {
        // Create special facets and page info which are empty or default
        const testSpecialFacets = createMap();
        const pageInfo = DefaultPageInfo;

        // Creating the query with the custom pageInfo and special facets
        const noFilterPropertiesQuery = new EsgfSearchQuery(null, pageInfo, testSpecialFacets);

        // Expected query and the actual
        const actual = noFilterPropertiesQuery.id;

        // Running the test
        expect(actual).toEqual(standardQuery);
    });

    test('query without any PageInfo should just work and like normal return the query', () => {
        // Creating the filter and property and then putting it in an array
        const filter = createFilter();
        const property = createProperty(filter);
        const properties = [property];

        // Create special facets which is empty
        const testSpecialFacets = createMap();

        // Creating the query with the custom properties and special facets
        const noPageInfoQuery = new EsgfSearchQuery(properties, null, testSpecialFacets);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + "[" + testName + "],";
        const actual = noPageInfoQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });

    test('query without any specialFacets should just work and like normal return the query', () => {
        // Creating the filter and property and then putting it in an array
        const filter = createFilter();
        const property = createProperty(filter);
        const properties = [property];

        // Creating PageInfo which is default
        const pageInfo = DefaultPageInfo;

        // Creating the query with the custom properties and pageInfo
        const noSpecialFacetQuery = new EsgfSearchQuery(properties, pageInfo, null);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + "[" + testName + "],";
        const actual = noSpecialFacetQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });

    test('adding specialFacets should return query with specialFacets', () => {
        // Creating an empty query and Map
        const emptyQuery = new EsgfSearchQuery([]);
        const testMap = new Map<string, string>();

        // Add a special facet to the Map and then make it the specialFacets
        testMap.set(testShortName, testName);
        emptyQuery.specialFacets = testMap;

        // Expected query and the actual
        const expected = standardQuery + testShortName + '=' + testName;
        const actual = emptyQuery.id;

        // Running the test
        expect(actual).toBe(expected);
    });

    test('properties with different names should be put on alphabetic order', () => {
        // Creating Filters
        const filter = createFilter();

        // Creating the properties and putting them in an array
        const property1 = createProperty(filter, 'c');
        const property2 = createProperty(filter, 'a');
        const property3 = createProperty(filter, 'b');
        const properties = [property1, property2, property3];

        // Creating the query with the properties
        const differentPropertiesQuery = new EsgfSearchQuery(properties);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + "[" + testName + 'a,' + testName + 'b,' + testName + 'c' + "],";
        const actual = differentPropertiesQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });

    test('filters with different names should be put on alphabetic order', () => {
        // Creating Filters
        const filter1 = createFilter('c');
        const filter2 = createFilter('a');
        const filter3 = createFilter('b');

        // Creating the properties and putting them in an array
        const property1 = createProperty(filter1);
        const property2 = createProperty(filter2);
        const property3 = createProperty(filter3);
        const properties = [property1, property2, property3];

        // Creating the query with the properties
        const differentFiltersQuery = new EsgfSearchQuery(properties);

        // Expected query and the actual
        const expectedQuery = standardQuery + testShortName + 'a[' + testName + "],"
                                            + testShortName + 'b[' + testName + "],"
                                            + testShortName + 'c[' + testName + "],";
        const actual = differentFiltersQuery.id;

        // Running the test
        expect(actual).toEqual(expectedQuery);
    });
});
